package GCPlugins::GCgames::GCAmazonDE;

###################################################
#
#  Copyright 2005-2010 Christian Jodar
#  Copyright 2017-2022 Kerenoc
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCgames::GCgamesAmazonCommon;

{
    package GCPlugins::GCgames::GCPluginAmazonDE;

    use base 'GCPlugins::GCgames::GCgamesAmazonPluginsBase';
    use Encode;
    use URI::Escape;

    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();
        bless ($self, $class);

        $self->{suffix} = 'de';

        $self->initTranslations;
        
        return $self;
    }

    sub initTranslations
    {
        my $self = shift;
        
        $self->{translations} = {
            publisher     => "Verlag:",
            publication   => "Audible.de Erscheinungsdatum:",
            language      => "Sprache:",
            isbn          => "ISBN",
            dimensions    => "e und/oder Gewicht",
            series        => "Series",
            pages         => "Seiten",
            by            => "von",
            product       => "(Produktinformation|Produktbeschreibungen)",
            details       => "Technische Details",
            additional    => "Zusätzliche Produktinformationen",
            end           => "Feedback",
            sponsored     => "Gesponsert",
            description   => "Produktbeschreibungen",
            author        => "Autor",
            translator    => "bersetzer",  # Übersetzer
            artist        => "Illustrator",
            platform      => "(Platform|Plattform)",
            end           => "(Kundenfragen|Kundenrezensionen)",
        };
    }

    sub getName
    {
        return 'Amazon (DE)';
    }
    
    sub getLang
    {
        return 'DE';
    }

}

1;
