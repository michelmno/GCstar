# GCstar Plugins
Current status of plugins. A *nok* status means that the plugins is not working as expected but the the website is still available and that the plugin could be repared. A *ko* status means that the web site disappeared. Any help is welcome to improve or fix plugins. 

## Board games

| Plugin        | Lang | Authors           | Status | Comment |
| ------        | ---- |-------            | ------ | ------- |
| BoardGamegeek | EN   | Zombiepig         | ok     |         |
| TricTrac      | FR   | Florent - Kerenoc | ok     |         |
|~~ReservoirJeux~~ | FR   | Florent - Kerenoc | ok     | closed site (removed) |


## Books

| Plugin               | Lang | Authors               | Status | Comment |
| ------               | ---  | ------                | -----  | ------- |
| Amazon BR            | PT   | Varkolak-Kerenoc-Knight Rider | ok     | |
| Amazon CA-UK-US      | EN   | Varkolak-Kerenoc      | ok     | |
| Amazon DE            | DE   | Varkolak-Kerenoc      | ok     | |
| Amazon FR            | FR   | Varkolak-Kerenoc      | ok     | |
| Bdphile              | FR   | Jonas/jojotux-Kerenoc | ok     | |
| Bokkilden            | NO   | Tian                  | ok     | |
| Chapitre             | FR   | TPF-Kerenoc           | ok     | |
| Google Books (MW)    | EN   | MW                    | ok     | to fix (covers) | 
| Le-Livre             | FR   | Varkolak              | ok     | |
| NooSFere             | FR   | Varkolak              | ok     | |
| AdLibris (FI)        | FI   | TPF                   | nok    | to fix | 
| AdLibris (SV)        | SV   | TPF                   | nok    | to fix | 
| BDGuest              | FR   | jpa31                 | nok    | to fix (Bedetheque comics) |
| BibliotekaNarodowa   | PL   | MW                    | nok    | require Javascript, to fix (ISBN search) |
| Bol                  | IT   | TPF-UnclePetros       | nok    | to fix (moved to Mondadori) |
| Buscape              | PT   | TPF                   | nok    | to fix | 
| Casadelibro          | ES   | PPF                   | nok    | to fix | 
| Doubanbook 豆瓣      | ZH   | BW                    | nok    | API v2 requires a key|
| Fnac (FR)            | FR   | TPF - Kerenoc         | nok    | to fix : protection system |
| Fnac (PT)            | PT   | TPF                   | nok    | to fix |
| InternetBookshop IBS | IT   | TPF                   | nok    | to fix (moved to IBS.it) |
| ISBNdb               | EN   | TPF                   | nok    | to fix | 
| Merlin               | PL   | WG                    | nok    | to fix | 
| NUKat                | PL   | WG                    | nok    | to fix |
| Saraiva              | PT   | Nurev                 | nok    | to fix |
| ~~Alapage~~            | FR   | TPF                 | ko     | closed site : removed |
| ~~InternetBokHandeln~~ | SV   | TPF                 | ko     | closed site : removed |
| ~~LiberOnWer~~         | IT   | TPF                 | ko     | closed site : removed |
| ~~Mareno~~             | PL   | WG                  | ko     | closed site : removed |


## Building toys
 
| Plugin   | Lang | Authors | Status | Comment |
| -------- | ---- | ------- | ------ | ------- |
| Brickset | EN   | Tian    | ok     |         |


## Coins

| Plugin         | Lang | Authors                      | Status     | Comment                             |
| ------         | ---- | -------                      | ------     | -------                             |
| Numista        | FR   | Michel_P                     | ok         | |


## Comics

| Plugin          | Lang | Authors                      | Status     | Comment                             |
| ------          | ---- | -------                      | ------     | -------                             |
| Bdphile         | FR   | Jonas/jojotux - Kerenoc      | ok         | migration from books plugin done    |
| Bedetheque      | FR   | Mckmonster - Jpa31 - Kerenoc | to improve | writer search not working           |
| MangaSanctuary  | FR   | Biggriffon                   | nok        | changes in web site                 |
| ~~ComicBookDb~~ | EN   | Zombiepig                    | no         | site closed in 2019                 |


## Films

| Plugin          | Lang | Authors                     | Status     | Comment |
| ------          | ---- | -------                     | ------     | ------- |
| Allmovie        | EN   | Zombiepig - Kerenoc         | ok         | 2 passes |
| Allocine        | FR   | Tian - Kerenoc              | ok         | 2 passes |
| Amazon DE       | DE   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon FR       | FR   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon UK       | EN   | Tian - Kerenoc              | ok         | common code to be shared |
| Amazon US       | EN   | Tian - Kerenoc              | ok         | specific code |
| Animator        | RU   | zserghei                    | ok         | presumed ok, not tested with cyrillic charset |
| AnimatorEN      | EN   | zserghei                    | ok         | to improve, minimum English version |
| Animeka.com     | FR   | MeV                         | ok         | |
| CSFD            | CZ   | Petr Gajduek                | ok         | |
| Douban 豆瓣     | ZH   | BW                          | nok        | API v2 requires a key |
| DVDEmpire       | EN   | FiXx                        | to improve | lots of missing fields |
| DVDfr           | FR   | MeV                         | ok         | |
| DVDPost         | FR   | MeV - Kerenoc               | ok         | |
| FilmAffinityEN  | EN   | Tian - PIN - FiXx - Kerenoc | ok         | |
| FilmAffinityES  | ES   | Tian - PIN - FiXx - Kerenoc | ok         | |
| FilmUP          | IT   | Tian                        | ok         | |
| FilmWeb         | PL   | Tian - Kerenoc              | to improve | |
| IBS - Internet Bookshop | IT   | t-storm             | ok         | |
| IMDb            | EN   | groms - snaporaz            | ok         | |
| Moviecovers     | FR   | 2emeagauche - P Fratczak    | ok | |
| MovieMeter      | NL   | MaTiZ                       | ok         | |
| OdeonHU         | HU   | Anonymous                   | ok         | |
| OFDb            | DE   | MeV                         | ok         | |
| AniDB           | EN   | MeV                         | nok        | to fix |
| AnimeNfo        | EN   | MeV                         | nok        | to fix |
| Beyaz Perde     | TR   | Zuencap                     | nok        | to fix |
| CinemaClock.com | FR   | MeV                         | nok        | to fix |
| CineMotions.com | FR   | MeV                         | nok        | to fix |
| Kinopoisk       | RU   | Nazarov Pavel               | nok        | to fix |
| Discshop.se     | SV   | Tian                        | nok        | to fix |
| DVDPost.be      | EN   | MeV                         | nok        | to fix, based on French version |
| NasheKino       | RU   | zserghei                    | nok        | to fix (cyrillic characters) |
| Onet            | PL   | Marek Cendrowicz            | nok        | to fix |
| Port.hu         | HU   | Anonymous                   | nok        | to fix |
| Stopklatka      | PL   | Marek Cendrowicz            | nok        | to fix |
| TheMovieDB      | DE EN ES FR | Zombiepig            | ok         | new key for the API v3|
| ~~CartelesPeliculas~~       | ES  | DoVerMan         | ko         | closed web site |
| ~~CartelesMetropoliGlobal~~ | ES  | DoVerMan         | ko         | closed web site |
| ~~CulturaliaNet~~           | ES  | MeV              | ko         | closed web site |
| ~~Mediadis~~                | EN  | Tian             | ko         | closed site |
| ~~MonsieurCinema.com~~      | FR  | MeV              | ko         | closed site |

## Gadgets

| Plugin               | Lang | Authors          | Status | Comment |
| ------               | ---  | ------           | -----  | ------- |
| Amazon CA-UK-US      | EN   | Varkolak-Kerenoc | ok     | |
| Amazon DE            | DE   | Varkolak-Kerenoc | ok     | |
| Amazon FR            | FR   | Varkolak-Kerenoc | ok     | |
| UPC item DB          | EN   | Kerenoc          | ok     | trial API, limit on calls per day |
 
## Music

| Plugin      | Lang | Authors | Status | Comment         |
| ------      | ---- | ------- | ------ | -------         |
| Douban      | ZH   | BW      | ok     |                 |
| MusicBrainz | EN   | Tian    | ok     |                 |
| Discogs     | FR   | TPF     | nok    | API keys to get |


## Music Courses

| Plugin      | Lang | Authors | Status | Comment         |
| ------      | ---- | ------- | ------ | -------         |
| Alfred      | EN   | FiXx    | ok     |                 |


## TV Episodes

| Plugin       | Lang        | Authors   | Status | Comment               |
| ------       | ----        | -------   | ------ | -------               |
| Tvdb         | EN ES FR IT | Zombiepig | ok     |                       |
| TheMovieDB   | DE EN ES FR | Kerenoc   | ok     | new key for the API v3|


## TV Series

| Plugin       | Lang         | Authors             | Status | Comment |
| ------       | ----         | -------             | ------ | ------- |
| TheMovieDB   | DE EN ES FR  | Zombiepig - Kerenoc | ok     |         |
| Tvdb         | EN ES FR IT  | Zombiepieg          | ok     |         |


## Video games
    
| Plugin        | Lang | Authors        | Status | Comment |
| ------        | ---- | -------        | ------ | ------- |
| Amazon (CA)   | EN   | TPF            | ok     |         |
| Amazon (DE    | DE   | TPF            | ok     |         |
| Amazon (FR)   | FR   | TPF            | ok     |         |
| Amazon (JA)   | JA   | TPF            | ok     |         |
| Amazon (UK)   | EN   | TPF            | ok     |         |
| Amazon (US)   | EN   | TPF            | ok     |         |
| GameSpot      | EN   | Tian - Kerenoc | ok     |         |
| JeuxVideoCom  | FR   | Tian - TPF     | ok     |         |
| MobyGames     | EN   | TPF            | ok     |         |
| JeuxVideoFr   | FR   | Tian           | nok    | some pages are broken |
| NextGames     | IT   | TPF            | nok    | site moved to videogames.it |
| ~~Alapage~~   | FR   | TPF            | ko     |         |
| ~~DicoDuNet~~ | FR   | TPF            | ko     |         |
| ~~Ludus~~     | IT   | TPF            | ko     |         |
|~~TheLegac~~   | DE   | TPF            | ko     | site closed before GDPR enforcement |
