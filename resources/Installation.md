# GCstar installation

GCstar is based on the Perl language and the GTK2 library.

## Warning

Before installing a new version of GCstar, don't forget to make some backups of all your collections!

## Linux

On some Linux distributions, GCstar is available as a package and can be installed using the application manager or with a command line. GCstar can be installed from the source code using the following instructions.

### Dependencies installation with *apt* and *cpan*

Execute the following commands (or similar commands according to the Linux distribution) to install Perl and some libraries

````
sudo apt-get install perl
sudo apt-get install libgtk2-perl
cpan install XML::Simple
cpan install DateTime::Format::Strptime
cpan install JSON
````

### Installation of the application

In the target directory for GCstar, execute the following commands to get a minimal version of GCstar

````
wget -O - https://gitlab.com/Kerenoc/GCstar/repository/Test/archive.tar.gz | tar xzf -
mv GCstar-Test-* GCstar
cd GCstar/gcstar/bin
perl gcstar
````
If GCstar was already installed, copy the **gcstar** script in the appropriate binaries directory (probably **/usr/bin**, check with the "which gcstar" command) and copy the **lib** directory in the libraries directory (probably **/usr/lib/gcstar** or **/usr/share/gcstar/lib**).

### Installation of optional modules

To get a fully functional version of GCstar (statistics, importation/exportation, archives), some additional Perl modules are required

````
Archive::Tar
Archive::Zip
Compress::Zlib
Date::Calc
Digest::MD5
GD
GD::Graph
GD::Text
Image::ExifTool
MIME::Base64
MP3::Info
MP3::Tag
Net::FreeDB
Ogg::Vorbis::Header::PurePerl
Time::Piece
````


## Windows 

Windows installers for GCstar are available for [1.7.2](https://gitlab.com/Kerenoc/GCstar/-/tags/v1.7.2) and [1.7.3](https://gitlab.com/Kerenoc/GCstar/-/tags/v1.7.3) versions. GCstar used to be installed from the source code using the following instructions but some external libraries are no longer available.


### Manual installation using *ActivePerl* 

*Warning: the repository www.sisyphusion.tk is currently unavailable!*

- Install Perl from the ActivePerl website
- Add a package repository: ````ppm repo add http://www.sisyphusion.tk/ppm/````
- Install the GTK2 library :  ````ppm install gtk2````
- Extract an archive of the current dev version ````https://gitlab.com/Kerenoc/GCstar/repository/Test/archive.zip````
- From the newly created directory go into the ````gcstar\bin```` sub-directory
- Execute ````perl gcstar````

### Manual installation using *Strawberry Perl*

- Follow the first part of the instructions available on GitHub ````https://github.com/tothi/gcstar-win32````
 
### Updating after installation ###

- Install GCstar using the installer
- Download the [latest source code](https://gitlab.com/Kerenoc/GCstar/-/archive/main/GCstar-main.zip for the Gtk3 version) from Gitlab
- Unzip the archive in a temporary folder
- Copy the sub-folder **gcstar/lib/gcstar** under this folder to the sub-folder **lib** under the folder where you installed GCstar (for example "c:\Program Files\GCstar\lib")
- Run GCstar to benefit from the updates and latest plugins


