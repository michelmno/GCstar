# Migration from GTK2 to GTK3
As Gtk2 is no longer supported on Debian and Ubuntu, the migration was initiated to keep GCstar up-to-date. 

## Process

- [x] Try to rapidly achieve a version with the major screens and dialog
- [x] Commenting out some non essential code with #GTK to come back later to fix it
- [x] Identify systematic refactoring (from the docs)
- [ ] Test on Windows, Linux, MacOS

## Status

- refactoring for tooltips (set_tip to set_tooltip on widget)
- change Combo to ComboBox or ComboBoxText
- some functions changes by prefixing with set_ or get_
- some class methods call changed from -> to ::
- changes to RadioButton
- adaptation to models where tables are missing cols or rows
- models fixed for missing cols or rows attributes
- panels begin to show for some models
- some menus and dialog begin to work
- importation seems ok
- changing picture
- get_allocation changes
- window => get_window
- get_child => get_children[0]
- problem with get_active_iter returning defined value for empty list (ComboBox bug?)
- statistics display using a temporary file for image
- use TargetEntry for drag and drop
- changes for iter_next for TreeModel
- changes for get_selected_rows
- image list displayed
- popup menu displayed
- Table widget replaced by Grid
- restore acceleration map ok (with substitute to Gtk::Stock)
- workaround in AllMovie to Gtk3::SimpleList being more strict with columns
- compact and read only mode (deprecated hide_all)
- color selection (changes from colorsel to get_color_selection)
- stars for ratings (using Cairo)
- checked text (change signal insert-text to inserted-text)
- history stats fixed
- history text fields working
- workaround a bug in FileFilter::add_custom (seg fault on Linux)
- size of popup for images
- adapt to Gtk3 evolutions for stock resources (Gtk3::Stock)
- migrate themes when possible (deprecated Gtk3::RcStyle)
- fix CriticalDialog not opening when pb with files
- fix dynamic resizing of images list

## To check and solve

- append_page_menu, append_page
- ->window->set_cursor
- importation dialog not closing
- warnings from Gtk3.pm and Glib/Object/Introspection.pm
- displayInWindow method call inconsistent and {itemWindow} fields not used
- inactive up key in search results list (use down key first)

## Gtk3 design choices ##

- a popup menu opens with the current selection close to the mouse position (lot of blank space in some menus)











