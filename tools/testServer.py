#!python
# -*- coding: utf-8 -*-
##############################################################
import time
import socket
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.bind(('0.0.0.0', 50007))

n = 0
serv.listen(5)
while True:
      time.sleep(2)
      conn, addr = serv.accept()
      print('Client connected')
      from_client = ''
      while True:
            data = conn.recv(4096)
            if not data: break
            print (data)
            time.sleep(2)
            response = '<result><title>Item %d</title></result>'%n
            print('>>>>> '+response)
            conn.send(response.encode())
            n += 1
            #conn.send("I am SERVER<br>".encode())
      conn.close()
      print ('client disconnected')
